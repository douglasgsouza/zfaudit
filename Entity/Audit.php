<?php

namespace Portal\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */

class Audit {

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @var string
     * @ORM\Column(name="created_by", type="varchar")
     */
    private $createdBy;

    /**
     * @var string
     * @ORM\Column(name="field_name", type="varchar")
     */
    private $fieldName;

    /**
     * @var string
     * @ORM\Column(name="data_type", type="varchar")
     */
    private $dataType;

    /**
     * @var string
     * @ORM\Column(name="before_value_string", type="varchar")
     */
    private $beforeValueString;

    /**
     * @var string
     * @ORM\Column(name="after_value_string", type="varchar")
     */
    private $afterValueString;

    /**
     * @var string
     * @ORM\Column(name="before_value_text", type="text")
     */
    private $beforeValueText;

    /**
     * @var string
     * @ORM\Column(name="after_value_text", type="text")
     */
    private $afterValueText;

    /**
     * @var unknown_type
     * @ORM\Column(name="before_value_crypt", type="blob")
     */
    private $beforeValueCrypt;

    /**
     * @var unknown_type
     * @ORM\Column(name="after_value_crypt", type="blob")
     */
    private $afterValueCrypt;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    public function getDataType()
    {
        return $this->dataType;
    }

    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    }

    public function getBeforeValueString()
    {
        return $this->beforeValueString;
    }

    public function setBeforeValueString($beforeValueString)
    {
        $this->beforeValueString = $beforeValueString;
    }

    public function getAfterValueString()
    {
        return $this->afterValueString;
    }

    public function setAfterValueString($afterValueString)
    {
        $this->afterValueString = $afterValueString;
    }

    public function getBeforeValueText()
    {
        return $this->beforeValueText;
    }

    public function setBeforeValueText($beforeValueText)
    {
        $this->beforeValueText = $beforeValueText;
    }

    public function getAfterValueText()
    {
        return $this->afterValueText;
    }

    public function setAfterValueText($afterValueText)
    {
        $this->afterValueText = $afterValueText;
    }

    public function getBeforeValueCrypt()
    {
        return $this->beforeValueCrypt;
    }

    public function setBeforeValueCrypt($beforeValueCrypt)
    {
        $this->beforeValueCrypt = $beforeValueCrypt;
    }

    public function getAfterValueCrypt()
    {
        return $this->afterValueCrypt;
    }

    public function setAfterValueCrypt($afterValueCrypt)
    {
        $this->afterValueCrypt = $afterValueCrypt;
    }
}