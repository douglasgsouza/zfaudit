<?php
namespace Portal\Entity;
use Doctrine\ORM\Mapping as ORM;
use Portal\Entity\AuditAnnotation as Audit;

/**
 * @ORM\Table(name="jos_users")
 * @ORM\Entity
 * @ORM\EntityListeners({"\Portal\Service\AuditListener"})
 */
class User {

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $tipo;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $block;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="users", fetch="EAGER")
     * @ORM\JoinColumn(name="acl_role_id", referencedColumnName="id")
     * @Audit
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="Modulo")
     * @ORM\OrderBy({"posicao" = "ASC"})
     * @ORM\JoinTable(name="jos_drive_modulos_users",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="modulo_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $permissoes;

    /**
     * @ORM\Column(name="altdh", type="datetime")
     */
    private $dhAlteracao;

    /**
     * @ORM\Column(name="altid", type="string")
     */
    private $alteradoPor;

    /**
     * @ORM\Column(name="incdh", type="datetime")
     */
    private $dhInclusao;

    /**
     * @ORM\Column(name="incid", type="string")
     */
    private $incluidoPor;

    /**
     * Retorna se o usuário é Admin
     * @return boolean
     */
    public function isAdmin()
    {
        if ($this->admin) {
            return true;
        } elseif ($this->role != null) {
            return $this->role->getAdmin();
        } else {
            return false;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function getDrive()
    {
        return $this->tipo == 'S';
    }

    public function getBlock()
    {
        return $this->block;
    }

    public function setBlock($block)
    {
        $this->block = $block;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getPermissoes()
    {
        return $this->permissoes;
    }

    public function setPermissoes($permissoes)
    {
        $this->permissoes = $permissoes;
    }

    public function getModulos()
    {
        $arrModulos = $this->permissoes;
        foreach ($arrModulos as $key => $val) {
            if ($val->getDeleted() || $val->getPosicao() == 0) {
                unset($arrModulos[$key]);
            }
        }

        return $arrModulos;
    }

    public function getAcessos()
    {
        $arrAcessos = $this->permissoes;
        foreach ($arrAcessos as $key => $val) {
            if ($val->getDeleted() || $val->getPosicao() > 0) {
                $arrAcessos->remove($key);
            }
        }

        return $arrAcessos;
    }

    public function temAcessoModulo($intIdModulo, $bolAdminBypass = true)
    {
        if ($this->isAdmin() && $bolAdminBypass) {
            return true;
        }

        foreach ($this->permissoes as $val) {
            if ($val->getId() == $intIdModulo) {
                return true;
            }
        }

        return false;
    }

    public function getDhAlteracao()
    {
        return $this->dhAlteracao;
    }

    public function setDhAlteracao($dhAlteracao)
    {
        $this->dhAlteracao = $dhAlteracao;
    }

    public function getAdmin()
    {
        return $this->admin;
    }

    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    public function getAlteradoPor()
    {
        return $this->alteradoPor;
    }

    public function setAlteradoPor($alteradoPor)
    {
        $this->alteradoPor = $alteradoPor;
    }

    public function getDhInclusao()
    {
        return $this->dhInclusao;
    }

    public function setDhInclusao($dhInclusao)
    {
        $this->dhInclusao = $dhInclusao;
    }

    public function getIncluidoPor()
    {
        return $this->incluidoPor;
    }

    public function setIncluidoPor($incluidoPor)
    {
        $this->incluidoPor = $incluidoPor;
    }
}