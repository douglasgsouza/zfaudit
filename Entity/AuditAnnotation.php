<?php
namespace Portal\Entity;

use Doctrine\ORM\Mapping\Annotation;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
final class AuditAnnotation implements Annotation
{

}