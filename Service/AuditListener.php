<?php
namespace Portal\Service;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Common\Annotations\AnnotationReader;

class AuditListener
{
    public function preUpdate($objEntity, PreUpdateEventArgs  $objEvent)
    {
        $strClass      = get_class($objEntity);
        $objMetadata   = $objEvent->getEntityManager()->getClassMetadata($strClass);
        $strTable      = $objMetadata->getTableName();

        $objMappingListener = new AuditMappingListener($strTable . '_audit');

        $objEvm = $objEvent->getEntityManager()->getEventManager();
        $objEvm->addEventListener('loadClassMetadata', $objMappingListener);

        $objAudit = $objEvent->getEntityManager()->getRepository('\Portal\Entity\Audit');

        $objReflection = $objMetadata->getReflectionClass();
        $arrProperties = $objReflection->getProperties();

        $arrPropertiesAudit = array();
        foreach ($arrProperties as $val) {
            var_dump($val->getDocComment());
        }

        $reader = new AnnotationReader();
        $classAnnotations = $reader->getClassAnnotations($objReflection);
        var_dump($classAnnotations);
        die;


        $arrChangeSet = $objEvent->getEntityChangeSet();
        foreach ($arrChangeSet as $key => $val) {

        }

        $objEvm->removeEventListener('loadClassMetadata', $objMappingListener);
        die;
    }
}