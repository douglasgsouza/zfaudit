<?php
namespace Portal\Service;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class AuditMappingListener
{
    private $tabela;

    public function __construct($strTabela)
    {
        $this->setTabela($strTabela);
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $objMetadata = $eventArgs->getClassMetadata();
        $objMetadata->setTableName($this->getTabela());
    }

    public function getTabela()
    {
        return $this->tabela;
    }

    public function setTabela($strTabela) {
        $this->tabela = $strTabela;
    }

}